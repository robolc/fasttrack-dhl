<?php
/*
Plugin name: FastTrack
Plugin URI: http://www.robolc.net
Description: Easy way to let custommers track their parcels.
Author: robolc.net
Author URI: http://www.robolc.net
Version: 0.1 (BETA)
Text Domain: fasttrack
Domain Path: /languages
*/
include(plugin_dir_path(__FILE__) . 'includes/FT_license_activator.php');

// load text domain for translation
function fasttrack_textdomain() {
    load_plugin_textdomain( 'fasttrack', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'fasttrack_textdomain' );

define( 'FTSetting', 'ft-setting' );

// Register style sheet.
add_action( 'wp_enqueue_scripts', 'ft_register_styles', 1 );
function ft_register_styles() {
    wp_register_style( 'fasttrackcss', plugin_dir_url( __FILE__ ) . 'assets/css/style.css' );
    wp_register_script( 'fasttrackjs', plugins_url( '/assets/js/js.js', __FILE__ ) );
    wp_register_style('et-googleFonts', 'https://fonts.googleapis.com/css?family=Raleway');
    wp_enqueue_style( 'et-googleFonts');
}


// adds options page
function ft_options_menu() {
    add_options_page( 'Fast Track', 'Fast Track', 'manage_options', FTSetting, 'FT_settings_page' );
}
add_action('admin_menu', 'ft_options_menu');


// settings page itself
function FT_settings_page(){
$license = get_option('ft_license_key');
$status = get_option('ft_license_status');
?>
<div class="wrap">
    <h2><?php _e('Plugin License Options'); ?></h2>
    <form method="post" action="options.php">

        <?php settings_fields('ft_license'); ?>

        <table class="form-table">
            <tbody>
            <tr valign="top">
                <th scope="row" valign="top">
                    <?php _e('License Key'); ?>
                </th>
                <td>
                    <input id="ft_license_key" name="ft_license_key" type="text" class="regular-text"
                           value="<?php esc_attr_e($license); ?>"/>
                    <label class="description" for="ft_license_key"><?php _e('Enter your license key'); ?></label>
                </td>
            </tr>
            <?php if (false !== $license) { ?>
                <tr valign="top">
                    <th scope="row" valign="top">
                        <?php _e('Activate License'); ?>
                    </th>
                    <td>
                        <?php if ($status !== false && $status == 'valid') { ?>
                            <span style="color:green;"><?php _e('active'); ?></span>
                            <?php wp_nonce_field('ft_nonce', 'ft_nonce'); ?>
                            <input type="submit" class="button-secondary" name="ft_license_deactivate"
                                   value="<?php _e('Deactivate License'); ?>"/>
                        <?php } else {
                            wp_nonce_field('ft_nonce', 'ft_nonce'); ?>
                            <input type="submit" class="button-secondary" name="edd_license_activate"
                                   value="<?php _e('Activate License'); ?>"/>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php submit_button(); ?>

    </form>
    <?php
    }
    
// saves all updated data to the database
    function ft_register_option() {
        // creates our settings in the options table
        register_setting('ft_license', 'ft_license_key', 'edd_sanitize_license' );
    }
    add_action('admin_init', 'ft_register_option');

    function ft_edd_sanitize_license( $new ) {
        $old = get_option( 'ft_license_key' );
        if( $old && $old != $new ) {
            delete_option( 'ft_license_status' ); // new license has been entered, so must reactivate
        }
        return $new;
    }

function ft_tacking_page() {
    // loads css file
    wp_enqueue_style('fasttrackcss');
    include(plugin_dir_path(__FILE__) . 'includes/FT_API_request.php');
    ?>

    <div class="fasttrack">
        <h1 class="title">Enter your tracking number</h1>
            <form method="GET" action=" ">
        <input class="tracking_input" name="ft_tracking_number">
        <input type="submit" id="ft_popup-button" class="track_button" name="" value="<?php _e('Track Parcel'); ?>"/>
        </form>
    </div>
    <div>
        <h1><?php echo ft_api_request();?></h1>
    </div>
        <?php    }
add_shortcode('fasttrack', 'ft_tacking_page');



function ft_api_request(){
            if($_SERVER['REQUEST_METHOD'] == 'GET'){
        if(isset($_GET["ft_tracking_number"])){
        $ft_popup_enable = 'block';
        }
        }
    $data = array();
    $tracking_number = $_GET["ft_tracking_number"];
    $zipcode = '';
    $response = wp_remote_get( 'https://api-gw.dhlparcel.nl/track-trace?key=' . $tracking_number . '%2B' . $zipcode );
    $body = wp_remote_retrieve_body( $response );
    $data = json_decode($body);
    foreach($data as $item){
    }
    $barcode = $item->{'barcode'};
    $receivername = $item->{'receiver'}->{'name'};
    $street = $item->{'receiver'}->{'address'}->{'street'};
    $housenumber = $item->{'receiver'}->{'address'}->{'houseNumber'};
    $postalcode = $item->{'receiver'}->{'address'}->{'postalCode'};
    $city = $item->{'receiver'}->{'address'}->{'city'};
    $countrycode = $item->{'receiver'}->{'address'}->{'countryCode'};
    wp_enqueue_style('fasttrackcss');
    wp_enqueue_script( 'fasttrackjs' );

        //DATA RECEIVED = The shipment is registered
        //UNDERWAY      = The parcel is being sorted
        //IN DELIVERY	= The parcel is in transit
        //DELIVERED     = Delivered at door or DHL ServicePoint
    $data_received = true;
    $underway = false;
    $out_for_delivery = false;
    $deliverd = false;

    foreach ($item->{'view'}->{'phases'} as $code) {
        if($code->{'phase'} == "DATA_RECEIVED") {
            foreach ($code->{'events'} as $event){
                if($event->{'key'} == "PRENOTIFICATION_RECEIVED") {
                    $data_received = true;
                }
            }
        }
        else if($code->{'phase'} == "UNDERWAY") {
            foreach ($code->{'events'} as $event){
                if($event->{'key'} == "PARCEL_ARRIVED_AT_LOCAL_DEPOT") {
                    $underway = true;
                }
            }
        }
        else if($code->{'phase'} == "IN_DELIVERY") {
            foreach ($code->{'events'} as $event){
                if($event->{'key'} == "OUT_FOR_DELIVERY") {
                    $out_for_delivery = true;
                }
            }
        }
        else if($code->{'phase'} == "DELIVERED") {
            foreach ($code->{'events'} as $event){
                if($event->{'key'} == "DELIVERED") {
                    $deliverd = true;
                }
            }
        }
    }
    //$data_received = true;
    //$underway = true;
    //$out_for_delivery = true;
    //$deliverd = true;
        if ($data_received == false || $underway == false && $out_for_delivery == false && $deliverd == false){
        $deliverytitle = "Pakket niet gevonden";
        $progressbar_status = "";
        $deliverydate = 'UNKNOWN';
    }
        else if ($data_received == true && $underway == false && $out_for_delivery == false && $deliverd == false){
        $deliverytitle = "Informatie ontvangen";
        $progressbar_status = "received";
        $deliverydate = date("l j F", strtotime($deliverydate));
    }
        else if ($data_received == true && $underway == true && $out_for_delivery == false && $deliverd == false){
        $deliverytitle = "Op het depot";
        $progressbar_status = "depot";
        $deliverydate = date("l j F", strtotime($deliverydate));
    }
        else if ($data_received == true && $underway == true && $out_for_delivery == true && $deliverd == false){
        $deliverytitle = "Bezorger is onderweg";
        $progressbar_status = "underway";
        $deliverydate = date("l j F", strtotime($deliverydate));
    }
        else if ($data_received == true && $underway == true && $out_for_delivery == true && $deliverd == true){
        $deliverydate = $item->{'deliveredAt'};
        $deliverytitle = "Bezorgd op";
        $progressbar_status = "deliverd";
        $deliverydate = date("l j F", strtotime($deliverydate));
    }
    ?>
<!-- The Modal -->
<div id="ft_popup" class="ft_popup" style="display:<?php echo $ft_popup_enable ?>">

  <!-- Modal content -->
  <div class="ft_popup-content">
      <span class="ft_popup-close">&times;</span>
      <div class="ft_details">
          <p class="ft_title">Volg de zending van robolc</p>
          <p class="ft_barcode">Barcode: <span style="color: #fab700;"><?php echo $barcode; ?></span></p>
      </div>
      <hr class="ft_status_progress_bar">
      <div class="ft_status_bar">
          <div style="grid-area: received;" class="ft_status_received <?php if($progressbar_status == "received") { echo "ft_status_selected"; } ?>">
              <img class="ft_status_image" src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/status_received.png'; ?>">
              <b class="ft_stats_tekst <?php if($progressbar_status == "received") { echo "ft_stats_tekst_enable"; } ?>">Received</b>
          </div>
          <div style="grid-area: depot;" class="ft_status_depot <?php if($progressbar_status == "depot") { echo "ft_status_selected"; } ?>">
              <img class="ft_status_image" style="width: 80px; margin-top: 20px;" src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/status_depot.png'; ?>">
              <b style="top: 25px;" class="ft_stats_tekst <?php if($progressbar_status == "depot") { echo "ft_stats_tekst_enable"; } ?>">Received</b>
          </div>
          <div style="grid-area: underway;" class="ft_status_underway <?php if($progressbar_status == "underway") { echo "ft_status_selected"; } ?>">
              <img class="ft_status_image" style="margin-top: 5px;" src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/status_underway.png'; ?>">
              <b style="top: 25px;" class="ft_stats_tekst <?php if($progressbar_status == "underway") { echo "ft_stats_tekst_enable"; } ?>">Received</b>
          </div>
          <div style="grid-area: deliverd;" class="ft_status_deliverd <?php if($progressbar_status == "deliverd") { echo "ft_status_selected"; } ?>">
              <img class="ft_status_image" src="<?php echo plugin_dir_url( __FILE__ ) . 'assets/images/status_deliverd.png'; ?>">
              <b class="ft_stats_tekst <?php if($progressbar_status == "deliverd") { echo "ft_stats_tekst_enable"; } ?>">Received</b>
          </div>
      </div>
      <div>
          <h1 class="ft_deliveryinformation_title"><?php echo $deliverytitle;?></h1>
          <b class="ft_deliveryinformation_date"><?php echo $deliverydate;?></b>
      </div>
      <div>
          <h1 class="ft_deliveryinformation_details_title">Receiver</h1>
          <div class="ft_deliveryinformation_details">
               <p class="ft_deliveryinformation_details_adress"><?php echo $receivername; ?></p>
               <p class="ft_deliveryinformation_details_adress"><?php echo $street .' '. $housenumber; ?></p>
               <p class="ft_deliveryinformation_details_adress"><?php echo $postalcode; ?></p>
               <p class="ft_deliveryinformation_details_adress"><?php echo $city;?></p>
               <p class="ft_deliveryinformation_details_adress"><?php echo $countrycode; ?></p>
          </div>
      </div>
  </div>
</div>
<?php
}
